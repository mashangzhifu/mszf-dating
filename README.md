<h1 align='center'> 码上致富 交友APP,聊天交友,视频交友,语音交友,直播交友,同城交友,一对一交友,一对一视频聊天,1v1同城交友,1v1交友,付费交友,相亲交友,陌生人交友</h1>

## 介绍

该软件是一款支持多国语言的1v1聊天交友app，参考了市面上常见的一些交友APP开发的，目前开源的是第一版的模板。

前端是基于uniapp+tim+trtc
后端是java+mybatis plus+element ui

可用于交友APP,聊天交友,视频交友,语音交友,直播交友,同城交友,一对一交友,一对一视频聊天,1v1同城交友,1v1交友,付费交友,相亲交友,陌生人交友等等，支持定制，有需求的可联系我们定制，价格美丽，包你满意。

## 效果

![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszf-dating/20221021222012.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszf-dating/20221021222013.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszf-dating/20221021222014.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszf-dating/20221021222015.png)
![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/mszf-dating/20221021222016.png)

公众号：

![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/connect/qrcode.jpg)

客服微信：

![Image text](https://mszfgit.oss-cn-hangzhou.aliyuncs.com/connect/wx.jpg)