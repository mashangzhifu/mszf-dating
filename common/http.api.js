//定制开发请联系客服微信：csmszf

const http = uni.$u.http

// post请求，获取菜单
export const postMenu = (params, config = {}) => http.post('/ebapi/public_api/index', params, config)

// get请求，获取菜单，注意：get请求的配置等，都在第二个参数中，详见前面解释
export const getMenu = (data) => http.get('/ebapi/public_api/index', data)

export const login = (params, config = {dataType: 'json'}) => http.post('/member/auth/login', params, config)
export const logout = (params, config = {dataType: 'json'}) => http.post('/member/auth/logout', params, config)
export const register = (params, config = {dataType: 'json'}) => http.post('/member/auth/register', params, config)
export const sendSmsCode = (params, config = {dataType: 'json'}) => http.post('/member/auth/send-sms-code', params, config)
//export const upload = (params, config = {}) => http.upload('/app/file/upload', params, config)
export const deviceInit = (params, config = {}) => http.post('/app/common/device-init', params, config)
export const memberInfo = (params, config = {custom: { auth: true }}) => http.post('/app/member/info', params, config)
export const checkToken = (data) => http.get('/member/auth/check-token', data)
export const categoryList = (data) => http.get('/app/category/list', data)
export const goodInfo = (data) => http.get('/app/good/get', data)
export const createGood = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/good/create', params, config)
export const updateGood = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/good/update', params, config)
export const deleteGood = (data) => http.get('/app/good/delete', {...data, ...{custom: {auth: true}}})
export const goodPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/good/page', params, config)
export const upDownRack = (data) => http.get('/app/good/up-down-rack' , {...data, ...{custom: {auth: true}}})
export const dictList = (data) => http.get('/app/common/dict', data)
export const postInfo = (data) => http.get('/app/post/get', data)
export const createPost = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/post/create', params, config)
export const updatePost = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/post/update', params, config)
export const deletePost = (data) => http.get('/app/post/delete', data)
export const postPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/post/page', params, config)
export const postLike = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/post/like', params, config)
export const commentList = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/comment/list', params, config)
export const createComment = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/comment/create', params, config)
export const deleteComment = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/comment/delete', params, config)
export const likeComment = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/comment/like', params, config)
export const evaluatePage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/evaluate/page', params, config)
export const evaluateCreate = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/evaluate/create', params, config)

export const createFeedback = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/feedback/create', params, config)
export const createReport = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/report/create', params, config)
export const centerInfo = (data) => http.get('/app/center/info', data)
export const createFollow = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/follow/create', params, config)
export const deleteFollow = (data) => http.get('/app/follow/delete', data)
export const followPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/follow/page', params, config)
export const followList = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/follow/list', params, config)

export const attestInfo = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/attest/info', params, config)

export const createAttestZh = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/attest/zh/create', params, config)
export const updateAttestZh = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/attest/zh/update', params, config)

export const createAttestEn = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/attest/en/create', params, config)
export const updateAttestEn = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/attest/en/update', params, config)


export const createBrowse = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/browse/create', params, config)
export const browseList = (data) => http.get('/app/browse/list', data)
export const preOrder = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/order/pre', params, config)

export const createOrder = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/order/create', params, config)
export const deleteOrder = (data) => http.get('/app/order/delete', data)
export const orderPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/order/page', params, config)
export const orderInfo = (data) => http.get('/app/order/info', data)
export const orderCancel = (data) => http.get('/app/order/cancel', {...data, ...{custom: {auth: true}}})
export const orderDelete = (data) => http.get('/app/order/delete', {...data, ...{custom: {auth: true}}})
export const orderConfirm = (data) => http.get('/app/order/confirm', {...data, ...{custom: {auth: true}}})
export const orderPrice = (data) => http.get('/app/order/price', {...data, ...{custom: {auth: true}}})
export const orderShip = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/order/ship', params, config)

export const afterRefund = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/after/refund', params, config)
export const afterRefunds = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/after/refunds', params, config)
export const afterExchange = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/after/exchange', params, config)
export const afterInfo = (data) => http.get('/app/after/info', {...data, ...{custom: {auth: true}}})



export const prePay = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/pay/pre', params, config)
export const preInfo = (data) => http.get('/app/pay/info', {...data, ...{custom: {auth: true}}})
export const paypalCaptureOrder = (data) => http.get('/app/paypal/capture-order', {...data, ...{custom: {auth: true}}})

export const createAddress = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/address/create', params, config)
export const updateAddress = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/address/update', params, config)
export const deleteAddress = (data) => http.get('/app/address/delete', {...data, ...{custom: {auth: true}}})
export const addressPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/address/page', params, config)
export const addressInfo = (data) => http.get('/app/address/info',  {...data, ...{custom: {auth: true}}})
export const defAddress = (data) => http.get('/app/address/def', data)
export const walletPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/wallet/page', params, config)
export const walletInfo = (data) => http.get('/app/wallet/info', data)
export const walletRecharge = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/wallet/recharge', params, config)
export const walletWithdraw = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/wallet/withdraw', params, config)
export const timSigGen = (data) => http.get('/app/sig/gen', data)
export const configInfo = (params, config = {dataType: 'json'}) => http.post('/app/config/get', params, config)
export const newsPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/news/page', params, config)
export const newsInfo = (data) => http.get('/app/news/info', data)
export const newsCount = (data) => http.get('/app/news/count', data)
export const newsRead = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/news/read', params, config)
export const premiumPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/seller/premium', params, config)
export const newcomerPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/seller/newcomer', params, config)
export const rankingList = (params, config = {dataType: 'json'}) => http.post('/app/ranking/list', params, config)
export const nearbyPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/good/nearby', params, config)
export const attentionPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/good/attention', params, config)

export const updateNickName = (data) => http.get('/app/member/update_nickname', {...data, ...{custom: {auth: true}}})
export const updateIntro = (data) => http.get('/app/member/update_intro' , {...data, ...{custom: {auth: true}}})
export const updateArea = (data) => http.get('/app/member/update_area', {...data, ...{custom: {auth: true}}})
export const updateBirthday = (data) => http.get('/app/member/update_birthday', {...data, ...{custom: {auth: true}}})
export const updateSex = (data) => http.get('/app/member/update_sex', {...data, ...{custom: {auth: true}}})
export const updateAvatar = (data) => http.get('/app/member/update_avatar', {...data, ...{custom: {auth: true}}})
export const verifyNewMobile = (data) => http.get('/app/member/verify_new_mobile', {...data, ...{custom: {auth: true}}})
export const verifyMobile = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/member/update_mobile', params, config)

export const verifyNewMail = (data) => http.get('/app/member/verify_new_mail', {...data, ...{custom: {auth: true}}})
export const verifyMail = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/member/update_mail', params, config)

export const updatePassword = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/member/update_password', params, config)
export const updatePasswordEmail = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/member/update_password_mail', params, config)

export const upgrade = (data) => http.get('/app/version/upgrade', data)

export const googleOauth = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/oauth/google', params, config)
export const facebootOauth = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/oauth/facebook', params, config)

export const articleLike = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/article/like', params, config)
export const articlePage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/article/page', params, config)
export const articleInfo = (data) => http.get('/app/article/info',  {...data, ...{custom: {auth: true}}})

export const bannerList = (data) => http.get('/app/banner/list', {...data, ...{custom: {auth: true}}})

export const mailLogin = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/appore/login', params, config)
export const sendEmailCode = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/appore/send-email-code', params, config)
export const mailRegister = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/appore/register', params, config)

export const wordsPage = (params, config = {dataType: 'json',custom: {auth: true}}) => http.post('/app/words/page', params, config)

export const chatCheck = (data) => http.get('/app/chat/check', {...data, ...{custom: {auth: true}}})
export const chatInfo = (data) => http.get('/app/chat/get', {...data, ...{custom: {auth: true}}})
